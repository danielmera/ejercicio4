/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication23;

/**
 *
 * @author CompuStore
 */
public class Empleado {
    private int HorasTrabajadas;
    private int PrecioHoraNormal;
    private int PrecioHoraExtra;
    private int Salario;
    
    
    public Empleado(){
        this.PrecioHoraNormal= 15;
        this.PrecioHoraExtra= 22;
    }
    public void CalcularSalario(int HorasTrabajadas){
        if(HorasTrabajadas>35){
            Salario=HorasTrabajadas * PrecioHoraExtra;
        }else{
            Salario= HorasTrabajadas *PrecioHoraNormal;
            
            
        }
    }

    public int getSalario() {
        return Salario;
    }

    public void setSalario(int Salario) {
        this.Salario = Salario;
    }
}